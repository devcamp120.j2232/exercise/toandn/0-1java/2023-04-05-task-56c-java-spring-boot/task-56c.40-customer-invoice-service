package com.devcamp.restapicustomerinvoice.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;
import com.devcamp.restapicustomerinvoice.Controller.models.Customer;

@Service
public class CustomerService {
  ArrayList<Customer> customers = new ArrayList<>();
  Customer customer1 = new Customer(1, "Teo", 20);
  Customer customer2 = new Customer(2, "Binh", 30);
  Customer customer3 = new Customer(3, "Manh", 25);

  public ArrayList<Customer> getListCustomers() {
    customers.add(customer1);
    customers.add(customer1);
    customers.add(customer1);

    return customers;
  }
}
