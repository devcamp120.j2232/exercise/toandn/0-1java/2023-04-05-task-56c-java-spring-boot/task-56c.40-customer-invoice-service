package com.devcamp.restapicustomerinvoice.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapicustomerinvoice.Controller.models.Customer;
import com.devcamp.restapicustomerinvoice.service.CustomerService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CustomerController {
  @Autowired
  CustomerService customerService;

  @GetMapping("/customers")
  public ArrayList<Customer> getListCustomers() {
    return customerService.getListCustomers();
  }
}
